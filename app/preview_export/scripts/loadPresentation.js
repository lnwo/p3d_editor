var loadPresentationLocalStorage = function() {
	var presentation = localStorage.getItem('preview-string');
	var config = JSON.parse(localStorage.getItem('preview-config'));

	if (presentation) {
		//document.body.innerHTML = presentation;
                $(document.body).html(presentation);
	}
};

var loadPresentationSaved = function() {
	var presentation = window.presentation_data['preview-string'];
	var config = JSON.parse(window.presentation_data['preview-config']);

	if (presentation) {
                //document.body.innerHTML = presentation;
                $(document.body).html(presentation);
	}
};

if('presentation_data' in window){
	loadPresentation = loadPresentationSaved;
}else{
	loadPresentation = loadPresentationLocalStorage;
}