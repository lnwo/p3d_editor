define(['../locales/en',
		'handlebars'],
function(en, handlebars) {
	var langs = {
		en: en,
	};

	var lang = window.navigator.language || window.navigator.userLanguage;
	var result = langs[lang.split('-')[0]] || langs.en;
	handlebars.registerHelper("lang", function(key) {
		return result[key];
	});

	return result;
});
