define(['libs/backbone'],
function(Backbone) {
	return Backbone.View.extend({
		events: {
			destroyed: 'dispose',
			'click input[type=text]': '_textClicked',
			'click button.close': '_closeClicked',
			"keyup input[name=text]": "keyPressed",
		},

		className: "textCopy modal",

		initialize: function() {
			this.render = this.render.bind(this);
			this.template = JST['tantaman.web.widgets/TextCopyDialog'];
		},

		render: function(title, msg, value) {
			this.$el.html(this.template({
				title: title,
				msg: msg,
				value: value
			}));
			this.$el.modal('show');
			this.$el.find("input").focus();
			this.$el.find("input").select();
			return this;
		},

		dispose: function() {

		},

		keyPressed: function(e){
			if(e.which==13){
				this.$el.modal('hide');
			}
		},

		_textClicked: function(e) {
			this.$el.find("input").select();
		},


		constructor: function TextCopyModal() {
			Backbone.View.prototype.constructor.call(this);
		}
	});
});