/*
 @author Matt Crinklaw-Vogt
 */
define(['libs/backbone', 'common/Calcium'],
    function(Backbone, Calcium) {
        return Backbone.View.extend({
            audio_is_recording: false,
            audio_input: null,
            audio_recorder: null,
            audio_recorder_initialized: false,
            className: "urlShare modal hide",
            events: {
                "click .ok": "okClicked",
                "hidden": "hidden"
            },
            initialize: function() {
            },
            show: function(cb) {
                this.cb = cb;
                return this.$el.modal('show');
            },
            render: function(title,share_url,embed_url) {
                var _this = this;
                var options = {
                    REFERRER : encodeURIComponent(window.location.href),
                    TITLE: "Prezent3D",
                    URL: encodeURIComponent(share_url),
                    SHARE_URL: share_url,
                    EMBED_URL: embed_url
                };
                this.$el.html(JST["tantaman.web.widgets/URLShareModal"](options));
                this.$el.modal();
                this.$el.modal("show");
                this.$input = this.$el.find("input[name='url']");
                this.$el.find("#modal-title").text(title);
                this.$input.val(share_url);
                this._url = share_url;
                return this.$el;
            },
            constructor: function UrlShareModal() {
                Backbone.View.prototype.constructor.apply(this, arguments);
            },
        });
    });
