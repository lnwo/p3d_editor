define(['libs/backbone'],
function(Backbone) {
	return Backbone.View.extend({
		events: {
			destroyed: 'dispose',
			'click input[type=text]': '_textClicked',
			'click button.close': '_closeClicked',
			"keyup input[name=text]": "keyPressed",
		},

		className: "link modal",

		initialize: function() {
			this.render = this.render.bind(this);
			this.template = JST['tantaman.web.widgets/LinkModal'];
		},

		render: function(title, label, url, value) {
			this.$el.html(this.template({
				title: title,
				label: label,
                                url: url,
				value: value
			}));
			this.$el.modal('show');
			this.$el.find("input").focus();
			this.$el.find("input").select();
			return this;
		},

		dispose: function() {

		},

		keyPressed: function(e){
			if(e.which==13){
				this.$el.modal('hide');
			}
		},

		_textClicked: function(e) {
			this.$el.find("input").select();
		},


		constructor: function LinkModal() {
			Backbone.View.prototype.constructor.call(this);
		}
	});
});