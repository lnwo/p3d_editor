/*
@author Matt Crinklaw-Vogt
*/
define(['libs/backbone', 'common/Calcium', 'libs/imgup'],
function(Backbone, Calcium, Imgup) {
	var modalCache = {};
	var reg = /[a-z]+:/;
	var imgup = new Imgup();
	var ignoredVals = {
		'string': true
	};

	var Modal = Backbone.View.extend({
		audio_is_recording: false,
		audio_input: null,
		audio_recorder: null,
		audio_recorder_initialized: false,
		className: "itemGrabber modal hide",
		events: {
			"click .ok": "okClicked",
			"click li[data-choose-file-url]": "fileChooserClicked",
			"click button[data-option='browse']": "browseClicked",
                        "click button[data-option='find-external']": "findFilesClicked",
                        "click button[data-option='find-template']": "findTemplatesClicked",
                        "change input[data-option='load-external']": "findFilesFileChosen",
                        "change input[data-option='load-internal']": "findTemplatesFileChosen",
			"click button[data-option='list-files']": "listFilesClicked",
			"click button[data-option='record']": "recordClicked",
			"change input[type='file']": "fileChosen",
			"keyup input[name='itemUrl']": "urlChanged",
			"paste input[name='itemUrl']": "urlChanged",
			"hidden": "hidden"
		},
		initialize: function() {
			this.loadItem = _.debounce(this.loadItem.bind(this), 200);
		},
		show: function(cb) {
			this.cb = cb;
			return this.$el.modal('show');
		},
		okClicked: function() {
			if (!this.$el.find(".ok").hasClass("disabled")) {
				var options = {};
				var src = $('#src').val()
				this.cb(src,options);
				return this.$el.modal('hide');
			}
		},
		uploadHandler: function(iu){
			var _this = this;
			iu.progress(function(ratio) {
				_this._updateProgress(ratio);
			}).then(function(result) {
				_this._switchToThumbnail();
				if(result.success) {
					_this.$input.val(result.data.uploaded_link);
					_this.urlChanged({
						which: -1
					});
				}else{
					_this._updateProgress(0);
					if(result.data.error_msg)
						alert(result.data.error_msg);
				}
			}, function(result) {
				_this._updateProgress(0);
				_this._switchToThumbnail();
				_this.$input.val('Failed to upload file');
				if(result.data.error_msg)
					alert(result.data.error_msg);
			});
		},
		fileChosen: function(e) {
			var f, reader,
				_this = this;
			f = e.target.files[0];
			//if (!f.type.match('(image|video|audio).*'))
			//	return;

			this._switchToProgress();
			this.item.src = '';

			this.uploadHandler(imgup.upload(f,this.options.tag));

			// reader = new FileReader();
			// reader.onload = function(e) {
			//   _this.$input.val(e.target.result);
			//   _this.urlChanged({
			//     which: -1
			//   });
			// };
			// reader.readAsDataURL(f);
		},
		browseClicked: function() {
			return this.$el.find('input[type="file"]').click();
		},
                findFilesClicked: function(){
                        window.open("imgselector.html#callback=image_select_callback&param=itemUrl","SELECT_IMG");
		},
                findTemplatesClicked: function(){
                        window.open("templateimgselector.php#callback=template_img_select_callback&param=itemUrl","SELECT_IMG");
		},
                findFilesFileChosen: function(){
                    this._switchToProgress();
                    this.item.src = '';
                    this.uploadHandler(imgup.upload(this.$input.val(),this.options.tag));
                },
                findTemplatesFileChosen: function(){
                    this.urlChanged({which: -1});
                },
		fileChooserClicked: function(e){
			this.$input.val($(e.target).data("chooseFileUrl"));
			this.urlChanged({which: -1});
		},
		listFilesClicked: function() {
			var _this = this;
			imgup.list(this.options.tag).then(function(result) {
        				_this.$el.find('.server-files').show();
        				var list = _this.$el.find('.server-files ul');
        				if(result.success){
	        				if(result.files.length>0){
		        				list.html("");
		        				for(var i=0;i<result.files.length;i++){
		        					list.append("<li data-choose-file-url=\""+result.files[i].url+"\">"+result.files[i].name+"</li>");
		        				}
	        				}else{
	        					list.html("No file");
	        				}
	        			}else{
	        				list.html("Error requesting file list");
	        			}
        			});
			return false;
		},
		hidden: function() {
			if (this.$input != null) {
				//this.item.src = '';
				return this.$input.val("");
			}
		},
		urlChanged: function(e) {
			if (e.which === 13) {
				this.src = this.$input.val();
				return this.okClicked();
			} else {
				this.loadItem();
			}
		},
		loadItem: function() {
			var val = this.$input.val();

			if (val in ignoredVals)
				return;

			var r = reg.exec(val);
			if (r == null || r.index != 0) {
				val = 'https://' + val;
			}
			if(this.options.tag === "audio" || this.options.tag === "video"){
				try{this.item.pause();}catch(ex){};
			}
			this.item.src = val;
			if(this.options.tag === "audio" || this.options.tag === "video"){
				try{
                                        this.item.load();
					this.item.play();
				}catch(ex){}
			}
			return this.src = this.item.src
		},
		_itemLoadError: function() {
			this.$el.find(".ok").addClass("disabled");
			return this.$el.find(".alert").removeClass("dispNone");
		},
		_itemLoaded: function() {
			this.$el.find(".ok").removeClass("disabled");
			return this.$el.find(".alert").addClass("dispNone");
		},
		// should probably just make a sub component to handle progress
		_updateProgress: function(ratio) {
			this.$progressBar.css('width', ratio * 100 + '%');
		},
		_switchToProgress: function() {
			this.$thumbnail.addClass('dispNone');
			this.$progress.removeClass('dispNone');
		},
		_switchToThumbnail: function() {
			this.$progress.addClass('dispNone');
			this.$thumbnail.removeClass('dispNone');
		},
		render: function() {
			var _this = this;
			this.$el.html(JST["tantaman.web.widgets/ChartImportModal"](this.options));
			this.$el.modal();
			this.$el.modal("hide");
			this.$progress = this.$el.find('.progress');
			this.$progressBar = this.$progress.find('.bar');
			this.$thumbnail = this.$el.find('.thumbnail');
			return this.$el;
		},
		constructor: function ChartImportModal() {
			Backbone.View.prototype.constructor.apply(this, arguments);
		},


		//recordings
		recordClicked: function(){
			if(this.audio_is_recording) this.stop_recording();
			else this.start_recording();
		},
		start_recording: function(){
			var self = this;
			if(!this.audio_recorder_initialized){
				navigator.getUserMedia({audio: true}, function(s){self.startUserMedia(s);}, function(e) {
					alert('No live audio input: ' + e);
				});
			}else{
				this.audio_is_recording = true;
				this.audio_recorder.clear();
				this.audio_recorder.record();
				this.$el.find("button[data-option='record']").addClass("recording").html("Stop [0h 0m 0s]");
				this.audio_second_ticks = 0;
				this.record_updateTimeout = setInterval(function(){
					self.audio_second_ticks++;
					var audio_len = "" + Math.floor(self.audio_second_ticks/3600) + "h";
					audio_len += " " + Math.floor((self.audio_second_ticks%3600)/60) + "m";
					audio_len += " " + Math.floor((self.audio_second_ticks%60)) + "s";
					self.$el.find("button[data-option='record']").html("Stop ["+audio_len+"]");
				},1000);
			}
		},
		stop_recording: function(){
			if(this.audio_is_recording){
				this.audio_is_recording = false;
				var self = this;
				clearInterval(this.record_updateTimeout);
				this.$el.find("button[data-option='record']").removeClass("recording").html("Processing....").prop("disabled",true);
				this.audio_recorder.stop();
				this.audio_recorder.exportWAV(function(blob) {
                    var fileReader = new FileReader();
                    fileReader.onload = function() {
						var buffer = new Uint8Array(this.result),
							data = self.parseWav(buffer);
						self._mp3Worker = new Worker(window.VH_AUDIO_RECORDER_PATH + "mp3Worker.js");
						self._mp3Worker.postMessage({
							cmd: 'init',
							config: {
								mode: 3,
								channels: 1,
								samplerate: data.sampleRate,
								bitrate: data.bitsPerSample
							}
						});
						self._mp3Worker.postMessage({
							cmd: 'encode',
							buf: self.Uint8ArrayToFloat32Array(data.samples)
						});
						self._mp3Worker.postMessage({
							cmd: 'finish'
						});
						self._mp3Worker.onmessage = function (event) {
							if (event.data.cmd == 'data') {
								self.$el.find("button[data-option='record']").html("Record").prop("disabled",false);
								var mp3Blob = new Blob([new Uint8Array(event.data.buf)], {type: 'audio/mp3'});
								var fileName = prompt("Enter file name","");
								fileName = fileName.replace(/["'<>;]/g,'') + ".mp3";
								self.uploadHandler(imgup.uploadBlob(mp3Blob,fileName,self.options.tag));
							}
						};
                    };
                    fileReader.readAsArrayBuffer(blob);
				});
			}
		},
		startUserMedia: function(stream){
			this.audio_input = window.audio_context.createMediaStreamSource(stream);
			this.audio_recorder = new Recorder(this.audio_input);
			this.audio_recorder_initialized = true;
			this.start_recording();
		},
        Uint8ArrayToFloat32Array: function (u8a) {
            var f32Buffer = new Float32Array(u8a.length);
            for (var i = 0; i < u8a.length; i++) {
                var value = u8a[i<<1] + (u8a[(i<<1) + 1]<<8);
                if (value >= 0x8000) {
                    value |= ~0x7FFF;
                }
                f32Buffer[i] = value / 0x8000;
            }
            return f32Buffer;
        },
        parseWav: function (wav) {
            function readInt(i, bytes) {
                var ret = 0,
                    shft = 0;

                while (bytes) {
                    ret += wav[i] << shft;
                    shft += 8;
                    i++;
                    bytes--;
                }
                return ret;
            }
            if (readInt(20, 2) != 1) {
                throw 'Invalid compression code ' + readInt(20, 2) + ', not PCM';
            }
            if (readInt(22, 2) != 1) {
                throw 'Invalid number of channels, not 1';
            }
            return {
                sampleRate: readInt(24, 4),
                bitsPerSample: readInt(34, 2),
                samples: wav.subarray(44)
            };
        },
	});

	return {
		get: function(options) {
			var cache_id = options.tag;
			if(typeof options.tag_id !== 'undefined') cache_id += options.tag_id;
			var previous = modalCache[cache_id];

			if (!previous) {
				previous = new Modal(options);
				previous.$el.bind('destroyed', function() {
					delete modalCache[cache_id];
				});

				modalCache[cache_id] = previous;

				previous.render();
				$('#modals').append(previous.$el);
			}

			return previous;
		},

		ctor: Modal
	};
});
