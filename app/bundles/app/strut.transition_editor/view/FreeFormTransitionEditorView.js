define(['libs/backbone',
	'strut/slide_snapshot/TransitionSlideSnapshot',
	'strut/deck/Utils'],
	function(Backbone, TransitionSlideSnapshot, DeckUtils) {
		'use strict';

		/**
		 * Transition editor, also called an "Overview mode", allows to manage slide transitions via drag-drop.
		 *
		 * @class TransitionEditorView
		 * @augments Backbone.View
		 */
		return Backbone.View.extend({
			className: 'slideTable strut-surface',
			events: {
				"click": "_clicked"
			},
			
			/*
			 TODO: render the slides...
			 The button bar will need to be taken care of
			 through something else...
			 Register button bars with a given mode?

			 strut.TransitionButtonProviders??

			 My model is a TransitionEditorModel
			 which contains a reference to the editorModel
			 and registry.
			 */

			/**
			 * Initialize transition editor view.
			 */
			initialize: function() {
				this._snapshots = [];

				this.model.deck().on('change:surface', this._surfaceChanged, this);
				// $(document.body).css('overflow', '');
			},

			/**
			 * React on deck surface class change.
			 *
			 * @param {Deck} deck
			 * @param {string} surface
			 * @private
			 */
			_surfaceChanged: function(deck, surface) {
				if (DeckUtils.isImg(surface)) {
					this.$el.css('background-image', DeckUtils.getImgUrl(surface));
				} else {
					this.$el.css('background-image', '');
					this.$el.removeClass();
					this.$el.addClass('slideTable strut-surface ui-selectable ' + surface);
				}
			},

			/**
			 * Event: user clicked editor background.
			 *
			 * @param {jQuery.Event} e
			 * @private
			 */
			_clicked: function(e) {
				this.model.deck().unselectSlides(null, true);
			},

			/**
			 * Remove transition editor view.
			 */
			remove: function() {
				this.dispose();
			},

			/**
			 * Dispose transition editor view.
			 */
			dispose: function() {
				Backbone.View.prototype.remove.call(this);
				this.model.deck().off(null, null, this);
			},

			/**
			 * Render transition editor.
			 *
			 * @returns {TransitionEditorView}
			 */
			render: function() {
				this.$el.selectable({
					filter: ".component",
					selected: function(event, ui) {
						$(ui.selected).trigger('select', ui);
					},
					unselected: function(event, ui) {
						$(ui.unselected).trigger('unselect', ui);
					}
				});

				this.$el.html('');
				var deck = this.model.deck();
				this._surfaceChanged(deck, deck.get('surface'));

				var colCnt = 6;
				var cnt = 0;
                                var lastX, lastY;
                                var lastLevel = 0;
                                var lastDeepLevel;
                                
				deck.get('slides').forEach(function(slide) {  
                                        var x, y;
                                        x = slide.get("x");
                                    
                                        if (x == null) {
                                            if(slide.get("subtype") == 'slide') {
                                                //slide.set("x", ((cnt % colCnt) | 0) * 280 + 180);
                                                //slide.set("y", ((cnt / colCnt) | 0) * 280 + 180);
                                                slide.set("x", cnt * 280 + 180);
                                                slide.set("y", 180);
                                            }
                                        }

                                        if(slide.get("subtype") == 'slide'){
                                            ++cnt;
                                        }else if(slide.get("subtype") == 'subslide'){
                                            if(slide.get("level") == lastLevel){
                                                slide.set("x", lastX);
                                                slide.set("y", lastY + 180);
                                                slide.set("z", slide.get("level") * -5000);
                                                lastDeepLevel = null;
                                            }else if(slide.get("level") > lastLevel+1){
                                                if(slide.get("level") == lastDeepLevel){
                                                    lastDeepLevel = slide.get("level");
                                                    slide.set("level", lastLevel);
                                                    slide.set("x", lastX);
                                                    slide.set("y", lastY + 180);
                                                    slide.set("z", slide.get("level") * -5000);
                                                }else{
                                                    lastDeepLevel = slide.get("level");
                                                    slide.set("level", lastLevel+1);
                                                    slide.set("x", lastX);
                                                    slide.set("y", lastY);
                                                    slide.set("z", slide.get("level") * -5000);
                                                }
                                            }else{
                                                slide.set("x", lastX);
                                                slide.set("y", lastY);
                                                slide.set("z", slide.get("level") * -5000);
                                                lastDeepLevel = null;
                                            }
                                        }
                                        lastX = slide.get("x");
                                        lastY = slide.get("y");      
                                        lastLevel = slide.get("level");    

                                        var snapshot = new TransitionSlideSnapshot({model: slide,
                                                registry: this.model.registry, deck: deck});
                                        this._snapshots.push(snapshot);
                                        this.$el.append(snapshot.render().$el);
				}, this);

				return this;
			},

			constructor: function TransitionEditorView() {
				Backbone.View.prototype.constructor.apply(this, arguments);
			}
		});
	});