define(['libs/backbone',
	'libs/moment',
	'libs/notify',
	'strut/header/model/HeaderModel',
	'strut/deck/Deck',
	'strut/slide_components/ComponentFactory',
	'common/Adapter',
	'tantaman/web/interactions/Clipboard',
	'./GlobalEvents',
	'tantaman/web/undo_support/CmdListFactory',
	'lang'],
	function(Backbone, moment, notify, Header, Deck, ComponentFactory, Adapter, Clipboard, GlobalEvents, CmdListFactory, lang) {
		'use strict';

		function adaptStorageInterfaceForSavers(storageInterface) {
			return new Adapter(storageInterface, {
				store: 'savePresentation',
				publish: 'saveExportedPresentation'
			});
		}

		return Backbone.Model.extend({
			initialize: function() {
				var _self = this;
				// is there a better way to do this?
				window.uiTestAcc = this;


				this._fontState = window.sessionMeta.fontState || {};
				this._deck = new Deck();
				this._deck.on('change:customBackgrounds', function(deck, bgs) {
					this.trigger('change:customBackgrounds', this, bgs)
				}, this);
				this.addSlide();

				this.set('header', new Header(this.registry, this));

				this.set('modeId', 'slide-editor');

				this.exportable = new Adapter(this, {
					export: 'exportPresentation',
					identifier: 'fileName',
					canAutoSave: 'canAutoSave'
				});

				this.exportable.adapted = this;

				var storageInterface = null;
				var storageInterface = this.registry.getBest('strut.StorageInterface');
				storageInterface = adaptStorageInterfaceForSavers(storageInterface);
				this._storageInterface = storageInterface;

				var savers = this.registry.getBest('tantaman.web.saver.AutoSavers');
				if (savers) {
					this._exitSaver = savers.exitSaver(this.exportable, storageInterface);
					this._timedSaver = savers.timedSaver(this.exportable, 20000, storageInterface);
				}

				this.clipboard = new Clipboard();
				this._createMode();

				this._cmdList = CmdListFactory.managedInstance('editor');
				GlobalEvents.on('undo', this._cmdList.undo, this._cmdList);
				GlobalEvents.on('redo', this._cmdList.redo, this._cmdList);

				Backbone.on('etch:state', this._fontStateChanged, this);
			},

			changeActiveMode: function(modeId) {
				if (modeId != this.get('modeId')) {
					this.set('modeId', modeId);
					this._createMode();
				}
			},

			customStylesheet: function(css) {
				if (css == null) {
					return this._deck.get('customStylesheet');
				} else {
					this._deck.set('customStylesheet', css);
				}
			},

			dispose: function() {
				throw "EditorModel can not be disposed yet"
				this._exitSaver.dispose();
				this._timedSaver.dispose();
				Backbone.off(null, null, this);
			},

			canAutoSave: function(){
				return this._deck.get('fileName') !== null;
			},

			setPrivate: function(isPriv){
				this._deck.set('is_private',+!!isPriv);
			},

			isPrivate: function(){
				return this._deck.get('is_private') || false;
			},

			newPresentation: function() {
				var num = window.sessionMeta.num || 0;

				num += 1;
				window.sessionMeta.num = num;

				this.importPresentation({
	        		fileName: null,
	        		slides: []
	      		});
                            window.open("templateselector.php#callback=template_select_callback&param=itemUrl","SELECT_IMG");	
                            this._deck.create();
			},

			/**
			 * see Deck.addCustomBgClassFor
			 */
			addCustomBgClassFor: function(color) {
				var result = this._deck.addCustomBgClassFor(color);
				if (!result.existed) {
					this.trigger('change:customBackgrounds', this, this._deck.get('customBackgrounds'));
				}
				return result;
			},

			customBackgrounds: function() {
				return this._deck.get('customBackgrounds');
			},

			importPresentation: function(rawObj) {
				// deck disposes iteself on import?
				console.log('New file name: ' + rawObj.fileName + ' | ' + rawObj.is_private);
                                this._deck.import(rawObj);
                                this._deck._renumberSlides();
			},

			exportPresentation: function(filename) {
				if (filename){
					this._deck.set('fileName', filename);
				}
				var obj = this._deck.toJSON(false, true);
				return obj;
			},

			fileName: function() {
				var fname = this._deck.get('fileName');
//				if (fname == null) {
//					fname = 'presentation-' + moment().format('YYYYMMDD_HHmmss');
//					this._deck.set('fileName', fname);
//					this._isNewFile = true;
//				}

				return fname;
			},

			deck: function() {
				return this._deck;
			},

			cannedTransition: function(c) {
				if (c != null)
					this._deck.set('cannedTransition', c);
				else
					return this._deck.get('cannedTransition');
			},

			slides: function() {
				return this._deck.get('slides');
			},

			addSlide: function(index,subtype,level,hierarchyNumber) {
                                this._deck.create(index,subtype,level,hierarchyNumber); 
			},                                                    
                        
                        duplicateSlide: function(index) {
				if (this.get('scope') == 'slideWell') {
					var slides = this._deck.selected;;
					this.clipboard.setItems(slides);
				}
				var slides = this.clipboard.getItems();
                                
                                if (slides != null && slides.length) {
                                    slides.forEach(function(slide){;
                                            var lastHierarchyNumber = slide.get("hierarchyNumber").split(".").pop();
                                            var nextHierarchyNumber = parseInt(lastHierarchyNumber)+1;
                                            var newHierarchyNumber = slide.get("hierarchyNumber").substring(0, slide.get("hierarchyNumber").length - lastHierarchyNumber.length)+""+nextHierarchyNumber;  
                                            slide.set("hierarchyNumber", newHierarchyNumber);
                                        
                                            var x = slide.get('x');
                                            if(x != null){
                                                if(slide.get("subtype") == 'slide'){
                                                    slide.set("x", slide.get("x") + 280);   
                                                }
                                            }
                                    }, this);
                                    this._deck.add(slides, index);
				}
			},  

			activeSlide: function() {
				return this._deck.get('activeSlide');
			},

			activeSlideIndex: function() {
				return this._deck.get('slides').indexOf(this._deck.get('activeSlide'));
			},

			addComponent: function(type) {
				var slide = this._deck.get('activeSlide');
				if (slide) {
					var comp = ComponentFactory.instance.createModel(type, {
						fontStyles: this._fontState
					});
					slide.add(comp);
				}
			},

			setExportedData: function(obj, shouldSave, cb){
				this._deck.set('exportedData',obj.data);
				if(typeof shouldSave !== 'undefined' && shouldSave === true){
					var exportID = this.fileName();
					if(!exportID){
                                                $.notify(lang['must_save'],'error');
                                        }else{
						this._storageInterface.publish(exportID, obj, function(success){
							if(success) cb(true,exportID);
							else cb(false);
						});
					}
				}
			},

			_fontStateChanged: function(state) {
				_.extend(this._fontState, state);
				window.sessionMeta.fontState = this._fontState;
			},

			_createMode: function() {
				var modeId = this.get('modeId');
				var modeService = this.registry.getBest({
					interfaces: 'strut.EditMode',
					meta: { id: modeId }
				});

				if (modeService) {
					var prevMode = this.get('activeMode');
					if (prevMode)
						prevMode.close();
					this.set('activeMode', modeService.getMode(this, this.registry));
				}
			},

			constructor: function EditorModel(registry) {
				this.registry = registry;
				Backbone.Model.prototype.constructor.call(this);
			},
                        
                        loadTemplate: function(templateId){
                            var storageInterface = this.registry.getBest('strut.StorageInterface');
                            var model = this;
                            storageInterface.loadDemo('template_' + templateId + '.strut', function(pres, err) {
                                    if (!err) {
                                            pres.fileName = null;
                                            model.importPresentation(pres);
                                    } else {
                                            console.log(err);
                                            console.log(err.stack);
                                    }
                            });
                        }
		});
	});
