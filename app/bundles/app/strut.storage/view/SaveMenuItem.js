define(['libs/backbone', '../model/ActionHandlers', 'tantaman/web/widgets/ErrorModal', 'lang', 'libs/notify'],
function(Backbone, ActionHandlers, ErrorModal, lang, notify) {
	return Backbone.View.extend({
		tagName: 'li',
		events: {
			click: 'save'
		},

		constructor: function SaveMenuItem(modal, model, storageInterface) {
			Backbone.View.prototype.constructor.call(this);
			this.model = model;
			this.saveAsModal = modal;
			this.storageInterface = storageInterface;
		},

		save: function() {
			fileName = this.model.fileName();
			if (fileName == null) {
				this.saveAsModal.show(ActionHandlers.save, lang.save_as);
			} else {
				ActionHandlers.save(this.storageInterface, this.model, fileName, function(success){
					if(success){
						$.notify(lang["notify_saved"].replace("{fileName}",fileName),"success");
					}else{
						$.notify(lang["notify_error"],"error");
						ErrorModal.show(false);
					}
				});
			}
		},

		render: function() {
			this.$el.html('<a>' + lang.save + '</a>');
			return this;
		}
	});
});