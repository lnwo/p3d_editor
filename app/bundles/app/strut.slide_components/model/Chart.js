define(['strut/deck/Component',
	'common/FileUtils'],
	function(Component, FileUtils) {
		'use strict';

		/**
		 * @class Chart
		 * @augments Component
		 */
		return Component.extend({
			initialize: function() {
				Component.prototype.initialize.apply(this, arguments);
				this.set('type', 'Chart');
			},

			constructor: function Chart(attrs) {
				Component.prototype.constructor.call(this, attrs);
			}
		});
	});