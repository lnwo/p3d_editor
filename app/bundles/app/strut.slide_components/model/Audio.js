define(['strut/deck/Component',
	'common/FileUtils'],
	function(Component, FileUtils) {
		'use strict';

		var matchers = [
			{
				type: 'html5',
				reg: /(.*)/,
				srcType: function(src) {
					return FileUtils.type(FileUtils.extension(src));
				}
			}
		];

		/**
		 * @class Audio
		 * @augments Component
		 */
		return Component.extend({
			initialize: function() {
				var matcher, regResult, _i, _len;
				Component.prototype.initialize.apply(this, arguments);
				this.set("type", "Audio");
				for (_i = 0, _len = matchers.length; _i < _len; _i++) {
					matcher = matchers[_i];
					regResult = matcher.reg.exec(this.get('src'));
					if (regResult != null) {
						this.set('shortSrc', regResult[1]);
						this.set('audioType', matcher.type);
						this.set('srcType', matcher.srcType(regResult[1]));
						this.set('srcName', (this.get("src")).replace(/^.*[\/\\=]/,'').replace(/\?.*$/,""));
						break;
					}
				}
				return this;
			},
			constructor: function Audio(attrs) {
				Component.prototype.constructor.call(this, attrs);
			}
		});
	});