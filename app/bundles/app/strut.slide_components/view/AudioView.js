define(["./ComponentView", './Mixers'],
	function(ComponentView, Mixers) {
		var Html5, result, types;

		/**
		 * @class AudioView.Html5
		 * @augments ComponentView
		 */
		Html5 = ComponentView.extend({
			className: "component audioView",

			/**
			 * Initialize VideoView.Html5 component view.
			 */
			initialize: function() {
				return ComponentView.prototype.initialize.apply(this, arguments);
			},

			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
				var $div,
					_this = this;
				ComponentView.prototype.render.call(this);
//				$div.append("<"+window.AUDIOJS_TEMPTAG+" controls " + (this.model.get("autoPlay")?"vh-autoplay":"") + " />");
//				$($div.children()[0]).append("<source src='" + (this.model.get("src")) + "' type='" + (this.model.get("srcType")) + "' />");
//				audiojs.newInstance($div.children()[0]);
				$div = $("<div class=\"ui360\"/>");
				$div.append($('<a />'));
				var $a = $div.children("a");
				$a.attr("href",this.model.get("src"));
				$a.text(this.model.get("srcName"));
				if(this.model.get("itemGlobal")){
					$div.addClass("audio-item-global");
				}
				if(this.model.get("autoPlay")){
					$div.addClass("audio-item-autoplay");
				}
				this.$el.find(".content").append($div);
				initAudioPlayer();
				return this.$el;
			}
		});
		types = {
			html5: Html5
		};
		return function(params) {
			return new types[params.model.get('audioType')](params);
		};
	});