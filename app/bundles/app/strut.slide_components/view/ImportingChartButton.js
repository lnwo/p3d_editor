define(['./ComponentButton', 'tantaman/web/widgets/ChartImportModal'],
	function(ComponentButton, ChartImportModal) {
		'use strict';

		/**
		 * @class ImportingComponentButton
		 * @augments ComponentButton
		 */
		return ComponentButton.extend({
			/**
			 * Initialize ImportingComponentButton.
			 */
			initialize: function() {
				ComponentButton.prototype.initialize.apply(this, arguments);

				this._modal = ChartImportModal.get(this.options);
				this._itemImported = this._itemImported.bind(this);
			},

			/**
			 * React on button click.
			 * @private
			 */
			_clicked: function() {
                                if(!window.CAN_CREATE_CHART){
                                        alert("Your current plan doesn't allow you to create charts.  You must upgrade your plan to create charts.");
                                }else{
                                        this._modal.show(this._itemImported);
                                }
			},

			/**
			 * Add importent component to the slide.
			 * @private
			 */
			_itemImported: function(src,options) {
				this.options.editorModel.addComponent($.extend({
					src: src,
					type: this.options.componentType,
				},options));
			},

			constructor: function ImportingChartButton() {
				ComponentButton.prototype.constructor.apply(this, arguments);
			}
		});
	})