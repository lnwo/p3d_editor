define(["./ComponentView"],
	function(ComponentView) {

		/**
		 * @class ChartView
		 * @augments ComponentView
		 */
		return ComponentView.extend({
			className: "component chartView",

			/**
			 * Initialize ChartView component view.
			 */
			initialize: function() {
				return ComponentView.prototype.initialize.apply(this, arguments);
			},

			/**
			 * Render element based on component model.
			 *
			 * @returns {*}
			 */
			render: function() {
                                //var scale;
				ComponentView.prototype.render.call(this);
				this.$el.find(".content").append(this.model.get('src'));
				//this.$el.append('<div class="overlay"></div>');
				//scale = this.model.get('scale');
				//this.$el.css({
				//	width: 960 * scale.x,
				//	height: 768 * scale.y
				//});
				return this.$el;

			}
		});
	});