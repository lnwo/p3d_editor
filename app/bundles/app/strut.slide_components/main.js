define(['./view/ComponentButton',
	'./view/ImportingComponentButton',
        './view/ImportingChartButton',
	'./model/Image',
	'./model/TextBox',
        './model/Chart',
	'./model/WebFrame',
	'./model/Video',
	'./model/Audio',
	'./view/ImageView',
	'./view/TextBoxView',
        './view/ChartView',
	'./view/WebFrameView',
	'./view/VideoView',
	'./view/AudioView',
	'./ComponentFactory',
	'lang',
	'./view/ShapesDropdown',
	'./ShapeCollection',
	'./view/ShapeView',
	'./model/Shape'],
	function(Button,
			 ImportingComponentButton,
                         ImportingChartButton,
			 Image,
			 TextBox,
                         Chart,
			 WebFrame,
			 Video,
			 Audio,
			 ImageView,
			 TextBoxView,
                         ChartView,
			 WebFrameView,
			 VideoView,
			 AudioView,
			 ComponentFactory,
			 lang,
			 ShapesDropdown,
			 ShapeCollection,
			 ShapeView,
			 Shape) {
		var availableShapes = new ShapeCollection();
		var service = {
			createButtons: function(editorModel) {
				var buttons = [];

				buttons.push(new Button({
					componentType: 'TextBox',
					icon: 'icon-text-width',
					name: lang.text,
					editorModel: editorModel
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'Image',
					icon: 'icon-picture',
					name: lang.image,
					tag: 'img',
					title: lang.insert_image,
					editorModel: editorModel,
					browsable: true
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'Video',
					icon: 'icon-film',
					name: lang.video,
					tag: 'video',
					srcTag: 'source',
					title: lang.insert_video,
					editorModel: editorModel,
					browsable: true,
					ignoreErrors: true
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'Audio',
					icon: 'icon-volume-up',
					name: lang.audio,
					tag: 'audio',
					srcTag: 'source',
					title: lang.insert_audio,
					editorModel: editorModel,
					browsable: true,
					ignoreErrors: true
				}));
                                
                                buttons.push(new ImportingChartButton({
					componentType: 'Chart',
					icon: 'icon-signal',
					name: lang.chart,
					tag: 'chart',
					title: lang.insert_chart,
					editorModel: editorModel
				}));

				buttons.push(new ImportingComponentButton({
					componentType: 'WebFrame',
					icon: 'icon-globe',
					name: lang.website,
					tag: 'iframe',
					title: lang.insert_website,
					editorModel: editorModel
				}));

				buttons.push(new ShapesDropdown(
					availableShapes,
					JST['strut.slide_components/ShapesDropdown'],
					{class: 'group-dropdown',
						editorModel: editorModel}      
				));
                        
				return buttons;
			}
		};

		return {
			initialize: function(registry) {
				// Register each component as a service
				// so it can be picked up by the ComponentFactory
				// If 3rd parties want to add components
				// then they just add their components to the registry as well.
				registry.register({
					interfaces: 'strut.ComponentButtonProvider'
				}, service);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Image'
					}
				}, Image);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'TextBox'
					}
				}, TextBox);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Chart'
					}
				}, Chart);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'WebFrame'
					}
				}, WebFrame);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Video'
					}
				}, Video);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Audio'
					}
				}, Audio);

				registry.register({
					interfaces: 'strut.ComponentModel',
					meta: {
						type: 'Shape'
					}
				}, Shape);
                                
				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Image'
					}
				}, ImageView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'TextBox'
					}
				}, TextBoxView);


                                registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Chart'
					}
				}, ChartView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'WebFrame'
					}
				}, WebFrameView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Video'
					}
				}, VideoView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Audio'
					}
				}, AudioView);

				registry.register({
					interfaces: 'strut.ComponentView',
					meta: {
						type: 'Shape'
					}
				}, ShapeView);

				ComponentFactory.initialize(registry);
			}
		};
	});