define(['libs/backbone', '../PreviewLauncher'],
function(Backbone, PreviewLauncher) {
	return Backbone.View.extend({
		className: 'btn-group iconBtns',
		events: {
			'click .act': '_launch',
			'click .publish': '_publish',
			'click .priv' : '_toggle_priv',
                        'click .save' : '_save',
		},

		initialize: function() {
			this._editorModel = this.options.editorModel;
			this._previewLauncher = new PreviewLauncher(this._editorModel);
			this._generators = this._editorModel.registry
				.getBest('strut.presentation_generator.GeneratorCollection');

			delete this.options.editorModel;
			// TODO: we should keep session meta per bundle...
			this._index = Math.min(window.sessionMeta.generator_index || 0, this._generators.length - 1);
			this._generatorChanged();

			this._template = JST['strut.presentation_generator/Button'];
		},

		_launch: function() {
			this._previewLauncher.launch(this._generators[this._index]);
		},

		_publish: function() {
			this._previewLauncher.publish(this._generators[this._index]);
		},
                
                _save: function() {
                       this._previewLauncher.save(this._generators[this._index])
                },

		_toggle_priv: function(){
			this._privmodeChanged(!this._editorModel.isPrivate());
		},

		_bind: function() {
			var self = this;
			this.$el.find('li').each(function(i) {
				var $btn = $(this);
				$btn.click(function(e) {
					// self._previewLauncher.launch(self._generators[i]);
					self.$el.find('.check').css('visibility', 'hidden');
					$btn.find('.check').css('visibility', '');
					self._index = i;
					window.sessionMeta.generator_index = i;
					self._generatorChanged();
					self.$el.find('.dropdown-toggle').dropdown('toggle');
					e.stopPropagation();
				});
			});
			var self = this;
			setTimeout(function(){
				self._privmodeChanged();
				self.listenTo(self._editorModel.deck(), "change:is_private", function(){ self._privmodeChanged(); });
			},500);
		},

		/**
		* Need to inform the world of a generator update.
		* Some modes are only present for certain generators.
		*/
		_generatorChanged: function() {
			this._editorModel.set('generator', this._generators[this._index]);
			if (this._$readout_save)
				this._$readout_save.text("Save");
                        if (this._$readout)
				this._$readout.text("Preview");
		},

		_privmodeChanged: function(value) {
			if(typeof value !== 'undefined'){
				if(!window.CAN_SAVE_PRIVATE) value=0;
				this._editorModel.setPrivate(value);
			}
			if(this._editorModel.isPrivate()){
				this._$privmode.text("Private");
			}else{
				this._$privmode.text("Public");
			}

			this._$privmode.prev("i").removeClass("icon-lock icon-globe").addClass(this._editorModel.isPrivate()?"icon-lock":"icon-globe");
			this._$privmode.parent("button").removeClass("btn-warning btn-info").addClass(this._editorModel.isPrivate()?"btn-warning":"btn-info");
			if(!window.CAN_SAVE_PRIVATE){
				this._$privmode.parent("button").prop("disabled",true);
			}else{
				this._$privmode.parent("button").prop("disabled",false);
			}
		},

		render: function() {
			this.$el.html(this._template({generators: this._generators, chosen: this._generators[this._index]}));
			this._bind();
                        this._$save = this.$el.find('.chosen_save');
			this._$readout = this.$el.find('.chosen');
			this._$privmode = this.$el.find('.chosen_priv');
			$(this.$el.find('.check')[this._index]).css('visibility', '');
			this._privmodeChanged();
			return this;
		}
	});
});