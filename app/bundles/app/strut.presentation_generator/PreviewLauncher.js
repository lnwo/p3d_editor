define([
		'libs/notify',
		'lang',
		'tantaman/web/widgets/LinkModal',
		'tantaman/web/widgets/URLShareModal',
                'strut/storage/view/StorageModal',
                'strut/storage/view/SaveMenuItem',
                'strut/storage/model/StorageInterface',
		'strut/storage/model/ActionHandlers'],
function(notify,lang,LinkModal,URLShareModal,StorageModal,SaveMenuItem,StorageInterface,ActionHandlers) {
	'use strict';
	var launch = 0;

	function PreviewLauncher(editorModel) {
		this._editorModel = editorModel;
		this._linkModal = new LinkModal();
		this._shareModal = new URLShareModal();
	};

	PreviewLauncher.prototype = {
		launch: function(generator) {
			if (window.previewWind)
				window.previewWind.close();

			this._editorModel.trigger('launch:preview', null);

			var previewStr = generator.generate(this._editorModel.deck());

			localStorage.setItem('preview-string', previewStr);
			localStorage.setItem('preview-config', JSON.stringify({
				surface: this._editorModel.deck().get('surface')
			}));

			window.previewWind = window.open(
				'preview_export/' + generator.id + '.html' + generator.getSlideHash(this._editorModel),
				window.location.href);
			var sourceWind = window;
		},
		publish: function(generator){
			var self = this;
			this._editorModel.trigger('launch:preview', null);
			var data = {
				gen_id: generator.id,
				preview: generator.generate(this._editorModel.deck()),
				surface: this._editorModel.deck().get('surface'),
				is_private: this._editorModel.isPrivate()
			};
			var previewData = {
				gen_id: generator.id,
				preview: generator.generate(this._editorModel.deck().previewDesk()),
				surface: this._editorModel.deck().get('surface'),
				is_private: this._editorModel.isPrivate()
			};               

                        this._editorModel.setExportedData({data: data, preview: previewData},true,function(success,publish_id){
                                if(success){
                                        $.notify(lang['notify_published'].replace("{publish_id}",publish_id),
                                        {
                                                className:"success",autoHideDelay:10000,
                                                clicked:function(){
                                                    if(data.is_private){
                                                        self._linkModal.render("Publish ID","Manage Permissions",window.SET_ACL_URL.replace('{ID}',publish_id),publish_id);
                                                    }
                                                    else
                                                    {
                                                        self._shareModal.render("Share",window.SHARE_URL.replace('{ID}',publish_id),window.EMBED_URL.replace('{ID}',publish_id));
                                                    }
                                                }
                                        });
                                        if(data.is_private){
                                            self._linkModal.render("Publish ID","Manage Permissions",window.SET_ACL_URL.replace('{ID}',publish_id),publish_id);
                                        }
                                        else
                                        {
                                            self._shareModal.render("Share",window.SHARE_URL.replace('{ID}',publish_id),window.EMBED_URL.replace('{ID}',publish_id));
                                        }    
                                }else{
                                        $.notify(lang['notify_published_error'],"error");
                                }
                        });
                        
                        /*if(!this._editorModel.fileName())
                        {    
                            var storageInterface = new StorageInterface(this._editorModel.registry);
                            var storageModal = new StorageModal({
                                    editorModel: this._editorModel,
                                    storageInterface: storageInterface
                            });
                            storageModal.render();
                            storageModal.show(ActionHandlers.save, lang.save_as); 
                        } */   
		},
                save: function(generator){
                    var storageInterface = new StorageInterface(this._editorModel.registry);
                    var storageModal = new StorageModal({
                            editorModel: this._editorModel,
                            storageInterface: storageInterface
                    });
                    storageModal.render();
                    var saveMenuItem = new SaveMenuItem(storageModal, this._editorModel, storageInterface);
                    saveMenuItem.save();
                }
	};

	return PreviewLauncher;
});