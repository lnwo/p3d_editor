// TODO: get rid of these.  Context buttons should be added
// based on available capabilities...
define(['./AddSlideButton', './DuplicateSlideButton', './SubslideButton',  'lang'],
function(AddSlideButton, DuplicateSlideButton, AddSubslideButton, lang) {
	'use strict';

	var service = {
		createButtons: function(editorModel, wellMenuModel) {
			var result = [];

			result.push(new AddSlideButton(editorModel, wellMenuModel));
                        result.push(new DuplicateSlideButton(editorModel, wellMenuModel));
                        result.push(new AddSubslideButton(editorModel, wellMenuModel));
                        
			return result;
		}
	};

	return {
		initialize: function(registry) {
			registry.register({
				interfaces: ['strut.WellContextButtonProvider']
			}, service);
		}
	}
});
