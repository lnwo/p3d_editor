define(['libs/backbone'],
function(Backbone) {
	return Backbone.View.extend({
		className: 'addBtn btn btn-success',
		events: {
			click: "_addSlide"
		},

		_addSlide: function() {
                        var previousSlide = this._wellMenuModel.editorModel.slides().models[this._wellMenuModel.slideIndex()-1];
                        if(previousSlide == null){
                            this._editorModel.addSlide(this._wellMenuModel.slideIndex(), "slide", 0, ""+1);
                        }else if(previousSlide.get("subtype") == "slide"){
                            var lastHierarchyNumber = previousSlide.get("hierarchyNumber").split(".").pop();
                            var nextHierarchyNumber = parseInt(lastHierarchyNumber)+1;
                            var newHierarchyNumber = previousSlide.get("hierarchyNumber").substring(0, previousSlide.get("hierarchyNumber").length - lastHierarchyNumber.length)+""+nextHierarchyNumber;       
                            this._editorModel.addSlide(this._wellMenuModel.slideIndex(), "slide", 0, newHierarchyNumber);
                        }else{
                            var lastHierarchyNumber = previousSlide.get("hierarchyNumber").split(".").pop();
                            var nextHierarchyNumber = parseInt(lastHierarchyNumber)+1;
                            var newHierarchyNumber = previousSlide.get("hierarchyNumber").substring(0, previousSlide.get("hierarchyNumber").length - lastHierarchyNumber.length)+""+nextHierarchyNumber; 
                            this._editorModel.addSlide(this._wellMenuModel.slideIndex(), "subslide", previousSlide.get("level"), newHierarchyNumber);
                        }
                            
		},

		render: function() {
			this.$el.html('<a href="#" data-toggle-"tooltip" title="Add Slide"><center><i class="icon-plus icon-white"></i></center></a>');
			return this;
		},

		constructor: function AddSlideButton(editorModel, wellMenuModel) {
			this._editorModel = editorModel;
     		this._wellMenuModel = wellMenuModel;
			Backbone.View.prototype.constructor.call(this);
		}
	});
});
