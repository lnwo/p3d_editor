define(['libs/backbone'],
function(Backbone) {
	return Backbone.View.extend({
		className: 'addBtn btn btn-success',
		events: {
			click: "_duplicateSlide"
		},

		_duplicateSlide: function() {
			this._editorModel.duplicateSlide(this._wellMenuModel.slideIndex());
		},

		render: function() {
			this.$el.html('<a href="#" data-toggle-"tooltip" title="Duplicate Slide/Subslide"><center><i class="icon-arrow-down icon-white"></i></center></a>');
			return this;
		},

		constructor: function CopySlideButton(editorModel, wellMenuModel) {
			this._editorModel = editorModel;
     		this._wellMenuModel = wellMenuModel;
			Backbone.View.prototype.constructor.call(this);
		}
	});
});
