define(['libs/backbone'],
function(Backbone) {
	return Backbone.View.extend({
		className: 'addBtn btn btn-success',
		events: {
			click: "_addSubslide"
		},

		_addSubslide: function() {
                        var previousSlide = this._wellMenuModel.editorModel.slides().models[this._wellMenuModel.slideIndex()-1];
                        if(previousSlide){
                            this._editorModel.addSlide(this._wellMenuModel.slideIndex(), "subslide", previousSlide.get("level")+1, previousSlide.get("hierarchyNumber")+".1");
                        }else{
                            this._editorModel.addSlide(this._wellMenuModel.slideIndex(), "slide", 0, ""+1);
                        }
		},

		render: function() {
			this.$el.html('<a href="#" data-toggle-"tooltip" title="Add Subslide"><center><i class="icon-zoom-in icon-white"></i></center></a>');
			return this;
		},

		constructor: function AddSubslideButton(editorModel, wellMenuModel) {
			this._editorModel = editorModel;
     		this._wellMenuModel = wellMenuModel;
			Backbone.View.prototype.constructor.call(this);
		}
	});
});
