/**
 * @author Matt Crinklaw-Vogt
 */
define(["common/Calcium", "./Slide"],
	function(Backbone, Slide) {

		// TODO Is this even used anywhere? If not, should probably be deleted.
		/**
		 * Comparator function for SlideCollection. Compares slides by their indexes.
		 * @see Backbone.Collection.comparator
		 *
		 * @param {Slide} l
		 * @param {Slide} r
		 * @returns {number}
		 */
		var slideComparator = function(l, r) {
			return l.get("index") - r.get("index");
		};

		/**
		 * @class SlideCollection
		 * @augments Backbone.Collection
		 */
		return Backbone.Collection.extend({
			model: Slide,

			/**
			 * Initialize collection model.
			 */
			initialize: function() {
				this.on("add", this._updateIndexes, this);
				this.on("remove", this._updateIndexes, this);
			},

			/**
			 * Update slide indexes on any changes to the contents of collection.
			 *
			 * @private
			 */
			_updateIndexes: function() {
				this.models.forEach(function(model, idx) {
					return model.set("index", idx);
				});
			},

			/**
			 * Update transition positions after slides have moved
			 *
			 * @param {Slide[]} slidesCopy
			 * @returns {SlideCollection} this
			 */
			slidesReorganized: function(slidesCopy) {
                                var transitions = [];                          
                                
				this.models.forEach(function(model, i) {
                                        transitions.push(slidesCopy[i].getPositionData());
				}, this);

				var silent = { silent: true };
				transitions.forEach(function(transition, i) {
                                        if(slidesCopy[i].get("subtype") == 'slide' && this.models[i].get("subtype") == 'slide'){
                                            this.models[i].set(transition, silent);
                                        }
				}, this);
                                
				this.models.forEach(function(model, i) {
					model.set('index', i);
				});

                                var cnt = 0;
                                var slides = this.models;
                                var lastX, lastY;
                                var lastLevel = 0;
                                var lastDeepLevel;

                                slides.forEach(function(slide) {
                                        var x, y;
                                        x = slide.get("x");

                                        if (x == null) {
                                            if(slide.get("subtype") == 'slide') {
                                                //slide.set("x", ((cnt % colCnt) | 0) * 280 + 180);
                                                //slide.set("y", ((cnt / colCnt) | 0) * 280 + 180);
                                                slide.set("x", cnt * 280 + 180);
                                                slide.set("y", 180);
                                            }
                                        }

                                        if(slide.get("subtype") == 'slide'){
                                            ++cnt;
                                        }else if(slide.get("subtype") == 'subslide'){
                                            if(slide.get("level") == lastLevel){
                                                slide.set("x", lastX);
                                                slide.set("y", lastY + 180);
                                                slide.set("z", slide.get("level") * -5000);
                                                lastDeepLevel = null;
                                            }else if(slide.get("level") > lastLevel+1){
                                                if(slide.get("level") == lastDeepLevel){
                                                    lastDeepLevel = slide.get("level");
                                                    slide.set("level", lastLevel);
                                                    slide.set("x", lastX);
                                                    slide.set("y", lastY + 180);
                                                    slide.set("z", slide.get("level") * -5000);
                                                }else{
                                                    lastDeepLevel = slide.get("level");
                                                    slide.set("level", lastLevel+1);
                                                    slide.set("x", lastX);
                                                    slide.set("y", lastY);
                                                    slide.set("z", slide.get("level") * -5000);
                                                }
                                            }else{
                                                slide.set("x", lastX);
                                                slide.set("y", lastY);
                                                slide.set("z", slide.get("level") * -5000);
                                                lastDeepLevel = null;
                                            }
                                        }
                                        lastX = slide.get("x");
                                        lastY = slide.get("y");      
                                        lastLevel = slide.get("level"); 
                                });
                                
				return this;
			},

			/**
			 * Change position of slides in SlideWell if their order is changed in collection.
			 *
			 * @param {Slide} l
			 * @param {Slide} r
			 * @private
			 */
			_swapTransitionPositions: function(l, r) {
				var silent, tempPosData;
				tempPosData = l.getPositionData();
				silent = {
					silent: true
				};
				l.set(r.getPositionData(), silent);
				r.set(tempPosData, silent);
			}
		});
	});

